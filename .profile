for file in ~/.{alias, functions}; do
    [ -r "$file" ] && source "$file"
done
unset file
export PATH=$HOME/bin:$PATH
