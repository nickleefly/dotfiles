# 1. Customized keyboard

    * go to https://ydkb.io to find a keyboard you like
    * go to the taobao shop to buy the controller
    * switch the controller
    * config you keyboard layers and layout from https://ydkb.io
    * flash the firmware to your new controller

---

# 2. Install fzf

    brew install fzf

## To install useful key bindings and fuzzy completion:

    $(brew --prefix)/opt/fzf/install

## Install ag, ripgrep & fd

    brew install the_silver_searcher
    brew install ripgrep
    brew install fd

---

# 3. Install tmux

- install tmux

  brew install tmux

- install reattach-to-user-namespace

  brew install reattach-to-user-namespace

---

# 4. run `./install.sh`

    ln -s $PWD/.profile .profile             || fail ".profile"
    ln -s $PWD/.tmux.conf .tmux.conf         || fail ".tmux.conf"
    ln -s $PWD/.functions .functions         || fail ".functions"
    ln -s $PWD/.alias .alias                 || fail ".alias"
    ln -s $PWD/.config/nvim ~/.config/nvim   || fail "nvim"

---

# 5. Check aliases

    # Easier navigation: .., ..., ~ and -
    alias ..="cd .."
    alias cd..="cd .."
    alias ...="cd ../.."
    alias ....="cd ../../.."
    alias .....="cd ../../../.."
    alias ~="cd ~" # `cd` is probably faster to type though
    alias -- -="cd -"

    alias h='history'
    alias now='date -u "+%Y-%m-%dT%H:%M:%SZ"'

    alias ag='ag -f --hidde'

    # force tmux to use 256 colors
    alias tmux='tmux -2'
    alias fuckit='git commit -am "$(curl -s http://www.whatthecommit.com/index.txt )"'

    #make tree a little cooler looking.
    alias tree="tree -CFa -I 'rhel.*.*.package|.git' --dirsfirst"

    # a friendlier delete on the command line
    alias emptytrash="find $HOME/.Trash -not -path $HOME/.Trash -exec rm -rf {} \; 2>/dev/null"

---

# 6. Install b for chrome bookmark search

    cp b $HOME/bin/b

---

# 7. Install neovim

    brew install neovim

---

# 8. tslide for readme

    npm i -g tslide
    tslide readme.md
